#include <cstdio>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define TO_RAD 0.01745329251
#define TO_DEG 57.2957795131

#include <chrono>

#include <cs/renderer/shader.hpp>
#include <cs/renderer/framebuffer.hpp>

#include <cs/utility/mesh_loader.hpp>
#include <cs/game/scene.hpp>
#include <cs/gui.hpp>

#include <cs/utility/log.hpp>

#include <string>

bool mouse_show = false;

#ifndef NDEBUG

cs::entity*     selected_entity     = nullptr;
cs::transform   buffer_transform {nullptr, nullptr};

ImVec4          color_buffer        = {};


char console_input_buffer[72] = "";
static void glfw_error_callback(int error, const char* description)
{
    std::fprintf(stderr, "Glfw Error %d: %s\n", error, description);
}

uint32_t    log_msg_mask = 0;
bool        log_msg_mask_selection[6] = {
    true,
    true,
    true,
    true,
    true,
    true,
};
#endif

auto last_frame = std::chrono::system_clock::now();
auto current_frame = std::chrono::system_clock::now();
auto frame_duration() {
    return std::chrono::duration_cast<std::chrono::microseconds>(current_frame - last_frame).count() / 1000000.0f;
}

namespace cs {
    struct strcmp {
        bool operator()(const char* a, const char* b) const {
            return ::strcmp(a, b) < 0;
        }
    };
}

/*
    Serialized Entity
    [Name]
    [Id]

    [Transform]
        [Parent ID]    (from transform)
        [Children IDs]  (from transform)
*/

void identify_entities(cs::entity* entity, std::map<cs::entity*, uint32_t>& entity_ids) {
    entity->id = entity_ids.size();
    entity_ids[entity] = entity->id;

    for (auto& c : entity->transform->children) {
        identify_entities(c->entity, entity_ids);
    }
}

void write_entity_to_file(cs::file& file, cs::entity* entity) {
    entity->write_to_file(file);

    for (auto c : entity->transform->children)
        write_entity_to_file(file, c->entity);
}

void serialize_scene(cs::file& f_scene, cs::scene *scene) {
    std::map<cs::entity*, uint32_t> entity_ids;
    identify_entities(scene->root, entity_ids);

    write_entity_to_file(f_scene, scene->root);
}

void read_entity_from_file(cs::file& file) {
    cs::scene scene("Test");
    
    uint32_t name_len = 0;
    file.read(&name_len);
    char* name = new char[name_len];
    file.read(name, name_len);
    const char* cname = name;
    cs::entity_type_e entity_type;
    file.read(&entity_type);

    if (entity_type == cs::entity_type_e::entity_e) {
        auto e1 = scene.add<cs::entity>(cname);
        cs::log::info("Loaded \"entity_e\"");
    }else if (entity_type == cs::entity_type_e::object_e) {
        cs::log::info("Loaded \"object_e\"");
    }else if (entity_type == cs::entity_type_e::player_e) {
        cs::log::info("Loaded \"player_e\"");
    }else if (entity_type == cs::entity_type_e::camera_e) {
        cs::log::info("Loaded \"camera_e\"");
    }else if (entity_type == cs::entity_type_e::light_e) {
        cs::log::info("Loaded \"light_e\"");
    }

    delete[] name;
}

int main(int argc, char *argv[]) {

#ifndef NDEBUG
    cs::window main_window(1920, 1080, "Debug");
    glfwSetErrorCallback(glfw_error_callback);

    cs::log::info("Starting engines ... ");
#else
    cs::window main_window(1920, 1080, "Release");
#endif

    cs::vec2 render_viewport_size = {1024, 576};

    cs::gui gui(main_window, "#version 130");

    std::map<const char*, cs::shader, cs::strcmp>   shaders;
    std::map<const char*, cs::mesh, cs::strcmp>     meshes;
    std::map<const char*, cs::texture, cs::strcmp>  textures;

    shaders["color_lighting"] = cs::shader();
    shaders["color_lighting"].add_shader_src("res/shaders/color_lighting/vertex.glsl", cs::vertex);
    shaders["color_lighting"].add_shader_src("res/shaders/color_lighting/fragment.glsl", cs::fragment);
    shaders["color_lighting"].compile();    

    shaders["simple_texture"] = cs::shader();
    shaders["simple_texture"].add_shader_src("res/shaders/simple_texture/vertex.glsl", cs::vertex);
    shaders["simple_texture"].add_shader_src("res/shaders/simple_texture/fragment.glsl", cs::fragment);
    shaders["simple_texture"].compile();

    cs::load_csof_into_mesh("res/models/cottage.csof",  meshes["cottage"]);
    cs::load_csof_into_mesh("res/models/human.csof",    meshes["human"]);

    textures["brick_wall"] = cs::texture("res/textures/wall.jpg");
    textures["test"] = cs::texture("res/textures/test.jpg");

    auto scene = new cs::scene("Scene 1", "res/scenes/scene1.cssf");

        auto player = scene->add<cs::player>("Player");
        player->transform->position = { 0, -10, 0 };

            auto player_mesh = player->add<cs::object>("Player mesh");
            player_mesh->shader     = &shaders["simple_texture"];
            player_mesh->mesh       = &meshes["human"];
            player_mesh->albedo     = &textures["test"];
            player_mesh->color      = {1.0, 0.7, 0.1};
            player_mesh->transform->scale = cs::vec3(0.012);

        auto camera_handle = player->add<cs::entity>("Camera handle");
            auto ortho_camera = camera_handle->add<cs::orthographic_camera>("Orthographic");
            ortho_camera->transform->position = cs::vec3({ 0.5, -1.3, 2 });

            auto perspective_camera = camera_handle->add<cs::perspective_camera>("Perspective");
            perspective_camera->transform->position = cs::vec3({0.5, -1.3, 2});

        auto camera_control = [&](float dt, float time){
            static cs::vec3 orientation_angles;
            cs::vec2 off = cs::get_mouse_delta();
            float sensitivity = dt * 0.1;
            off *= sensitivity;
            
            orientation_angles[0] += off[1];

            if(cs::is_key_pressed(GLFW_KEY_F5) && cs::is_key_pressed(GLFW_KEY_F5) < 2)
                if(scene->active_camera == perspective_camera)
                    scene->active_camera = ortho_camera;
                else
                    scene->active_camera = perspective_camera;

            if(scene->active_camera == perspective_camera)
            {
                if(orientation_angles[0] >  0.3) orientation_angles[0] =  0.3;
                if(orientation_angles[0] < -0.3) orientation_angles[0] = -0.3;
            }else
            {
                if(orientation_angles[0] >  85 * TO_RAD) orientation_angles[0] = 85 * TO_RAD;
                if(orientation_angles[0] < -85 * TO_RAD) orientation_angles[0] = -85 * TO_RAD;
            }

            scene->active_camera->transform->rotation = cs::from_euler_angles(orientation_angles);
        };

        camera_handle->update = camera_control;
        scene->active_camera = perspective_camera;

        auto cottage = scene->add<cs::object>("Cottage");
        cottage->transform->position    = {  0.0f, 0.0f, 0.0f };
        cottage->transform->scale       = 0.1f;
        cottage->color                  = { 0.7f, 0.4f, 0.6f };
        cottage->shader                 = &shaders["simple_texture"];
        cottage->mesh                   = &meshes["cottage"];
        cottage->albedo                 = &textures["brick_wall"];

    main_window.on_update = [&](float dt, float time){
        if(cs::is_key_pressed(GLFW_KEY_T))
            scene->update(dt / 10, time);
        else
            scene->update(dt, time);
    };

    main_window.on_render = [&](){
        scene->render();
    };


    cs::framebuffer main_framebuffer;
    // framebuffer configuration
    main_framebuffer.bind();

    // create a color attachment texture
    cs::texture main_colorbuffer(render_viewport_size);
    main_framebuffer.texture_2d(main_colorbuffer);

    // create a renderbuffer object for depth and stencil attachment (we won't be sampling these)
    cs::renderbuffer main_renderbuffer;
    main_renderbuffer.storage(render_viewport_size);
    main_renderbuffer.framebuffer();

    // now that we actually created the framebuffer and added all attachments we want to check if it is actually complete now
    if (main_framebuffer.check_status() != GL_FRAMEBUFFER_COMPLETE) cs::log::error("Main framebuffer is not complete!");

    main_framebuffer.unbind();

    cs::file f_scene("res/scenes/test.cssf", O_CREAT | O_RDWR);
    serialize_scene(f_scene, scene);

    read_entity_from_file(f_scene);

    float accumulator = 0.0f;
    auto dt = 1.0 / 60.0f;
    float time = 0.0f;    
    while(!main_window.should_close()) {
        current_frame = std::chrono::system_clock::now();
        auto delta = frame_duration();
        last_frame = current_frame;
        accumulator += delta;

        cs::disable_mouse(mouse_show);
        while (accumulator >= dt)
        {
            main_window.poll_events();

            main_window.update(dt, time);
            
            time += dt;
            accumulator -= dt;
        }
        
        main_framebuffer.bind();
        glEnable(GL_DEPTH_TEST);
            
            glViewport(0, 0, render_viewport_size[0], render_viewport_size[1]);
            glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

            main_window.render();
        
        glDisable(GL_DEPTH_TEST); 
        main_framebuffer.unbind();
        
        if(cs::is_key_pressed(GLFW_KEY_GRAVE_ACCENT) == 1) {
            mouse_show ^= 1;
        }

        if (mouse_show) {
            glfwSetInputMode(main_window.handle, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        }
        else {
            glfwSetInputMode(main_window.handle, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        }
        
        glClear(GL_COLOR_BUFFER_BIT);
#ifndef NDEBUG
        {
            gui.begin();

            ImGui::BeginMainMenuBar();
            auto menu_height = ImGui::GetWindowHeight();
            ImGui::SetNextItemWidth(800 / 3);
            if (ImGui::InputText("", console_input_buffer, 72, ImGuiInputTextFlags_EnterReturnsTrue)) {
                memset(console_input_buffer, 0, 72);
            }
            if (ImGui::MenuItem("Exit")) {
                main_window.close();
            }
            ImGui::EndMainMenuBar();

            ImVec2 viewport_window_position;
            ImVec2 viewport_window_size;
            {   // Viewport
                ImGui::Begin("Viewport", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
                ImGui::SetWindowPos({ main_window.width * 0.2f, menu_height });
                ImGui::SetWindowSize({ float(viewport_window_size[0]), float(viewport_window_size[1]) });

                viewport_window_position = ImGui::GetWindowPos();
                viewport_window_size = ImGui::GetWindowSize();

                ImGui::Image((void*)(intptr_t)main_colorbuffer.id_,
                    ImVec2(render_viewport_size[0], render_viewport_size[1]), { 0, 0 }, { 1, -1 });
                ImGui::SetWindowSize({ 0, 0 });
                ImGui::End();
            }
            ImVec2 data_window_size;
            {   // Scene hierarchy
                ImGui::Begin("Scene hierarchy", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);
                ImGui::SetWindowPos({0, menu_height});
                ImGui::SetWindowSize({ viewport_window_position.x, viewport_window_size.y});
                data_window_size = ImGui::GetWindowSize();

                ImGui::LabelText(scene->name.c_str(), "%s", scene->path.c_str());

                uint32_t    entity_num = 0;
                std::function<void(cs::entity*)> show_children = [&show_children, &entity_num](cs::entity* root) {
                    for (auto* c : root->transform->children) {
                        ImGuiTreeNodeFlags flags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_DefaultOpen;
                        entity_num++;
                        if (!c->children.size())            flags |= ImGuiTreeNodeFlags_Leaf;
                        if (c->entity == selected_entity)   flags |= ImGuiTreeNodeFlags_Selected;

                        if (ImGui::TreeNodeEx((void*)(intptr_t)entity_num, flags, "%d %s", c->entity->id, c->entity->name.c_str())) {
                            if (ImGui::IsItemClicked()) {
                                selected_entity = c->entity;
                            }

                            show_children(c->entity);
                            ImGui::TreePop();
                        }
                    }
                };

                show_children(scene->root);

                ImGui::End();
            }
            ImVec2 utility_window_position;
            ImVec2 utility_window_size;
            {   // Utility
                ImGui::Begin("Utility", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);

                ImGui::SetWindowPos({0, menu_height + viewport_window_size.y});
                ImGui::SetWindowSize({ main_window.width / 2.0f, main_window.height - menu_height * 5 - float(viewport_window_size[1])});

                utility_window_position = ImGui::GetWindowPos();
                utility_window_size = ImGui::GetWindowSize();

                ImGuiTabBarFlags tab_bar_flags = ImGuiTabBarFlags_None | ImGuiTabBarFlags_Reorderable;
                if (ImGui::BeginTabBar("MyTabBar", tab_bar_flags))
                {
                    if (ImGui::BeginTabItem("Log")) {
                        for (uint32_t i = 0; i < 6; ++i) {
                            char label[32] = "";
                            snprintf(label, 32, "%s", cs::log::log_msg_category_to_string(cs::log::log_msg_category(1 << i)));
                            
                            if(i) ImGui::SameLine();
                            if (ImGui::Checkbox(label, &(log_msg_mask_selection[i])));

                            if (log_msg_mask_selection[i]) {
                                log_msg_mask |= (1 << i);
                            }
                            else {
                                log_msg_mask &= ~(1 << i);
                            }
                        }

                        ImGui::Separator();

                        auto log_vector = cs::log::get_log();
                        for (auto log_msg : log_vector)
                        {
                            if (log_msg_mask & uint32_t(log_msg.category)) {
                                ImGui::Text("[%s] %s", cs::log::log_msg_category_to_string(log_msg.category), log_msg.text);
                            }
                        }

                        ImGui::EndTabItem();
                    }
                    ImGui::EndTabBar();
                }
                ImGui::End();
            }
            ImVec2 inspector_window_position;
            ImVec2 inspector_window_size;
            {   // Inspector
                ImGui::Begin("Entity inspector", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);

                ImGui::SetWindowPos({ viewport_window_position.x + viewport_window_size.x, menu_height});
                ImGui::SetWindowSize({ main_window.width - data_window_size.x - viewport_window_size.x, viewport_window_size.y});
                inspector_window_size = ImGui::GetWindowSize();
                inspector_window_position = ImGui::GetWindowPos();

                if (selected_entity != nullptr) {
                    ImGui::Text("Entity: %s", selected_entity->name);
                    ImGui::Text("Transform");
                    ImGui::Text("Currently inputting works only by pressing [ ENTER ]\n\t(hopefully soon TAB will work too)");
                    buffer_transform = *(selected_entity->transform);

                    if (ImGui::InputFloat3("Position", &(buffer_transform.position[0]), 4, ImGuiInputTextFlags_EnterReturnsTrue)) {
                        selected_entity->transform->position = buffer_transform.position;
                    }
                    auto orientation = buffer_transform.rotation.to_euler_angles() * TO_DEG;
                    if (ImGui::InputFloat3("Rotation", &(orientation[0]), 4, ImGuiInputTextFlags_EnterReturnsTrue)) {
                        selected_entity->transform->rotation = cs::from_euler_angles(orientation * TO_RAD);
                    }
                    buffer_transform.rotation = cs::from_euler_angles(orientation * TO_RAD);
                    if (ImGui::InputFloat3("Scale", &(buffer_transform.scale[0]), 4, ImGuiInputTextFlags_EnterReturnsTrue)) {
                        selected_entity->transform->scale = buffer_transform.scale;
                    }

                    if(selected_entity->entity_type == cs::entity_type_e::camera_e) {
                        auto selected_camera_entity = (cs::camera*)(selected_entity);

                        if(selected_camera_entity->camera_type == cs::camera_type_e::PERSPECTIVE) {
                            auto selected_perspective_camera_entity = (cs::perspective_camera*) selected_camera_entity;
                            
                            ImGui::Text("Perspective camera");
                            ImGui::SliderFloat("Field of view", &(selected_perspective_camera_entity->fov),             20, 100);
                            ImGui::SliderFloat("Z Far",         &(selected_perspective_camera_entity->z_far),           0, 10000);
                            ImGui::SliderFloat("Z near",        &(selected_perspective_camera_entity->z_near),          0, 1, "%.4f");
                        }else if(selected_camera_entity->camera_type == cs::camera_type_e::ORTHOGRAPHIC) {
                            auto selected_orthographic_camera_entity = (cs::orthographic_camera*) selected_camera_entity;  

                            ImGui::Text("Orthographic camera");                          
                        }
                    }else if(selected_entity->entity_type == cs::entity_type_e::object_e) {
                        auto selected_object_entity = (cs::object*)(selected_entity); 
                        
                        ImGui::Text("Object entity");
                        for(auto& m : meshes) {
                            if (&(m.second) == selected_object_entity->mesh) {
                                ImGui::Text("Mesh: %s", m.first);
                                break;
                            }
                        }

                        //ImGui::ColorPicker3("Object color", &(selected_object_entity->color[0]), ImGuiColorEditFlags_DisplayRGB);
                        if (selected_object_entity->albedo != nullptr) {
                            ImGui::SetNextItemWidth(250);
                            ImGui::Image((void*)(intptr_t)(selected_object_entity->albedo->id_),
                                ImVec2(150, 150), { 0, 0 }, { 1, -1 });
                        }
                    }else if(selected_entity->entity_type == cs::entity_type_e::player_e) {
                        auto selected_player_entity = (cs::player*)(selected_entity);
                     
                        ImGui::Text("Player entity");                       
                    }
                }

                ImGui::End();
            }
            {
                ImGui::Begin("Resource inspector", nullptr, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);

                ImGui::SetWindowPos({ utility_window_position.x + utility_window_size.x, utility_window_position.y});
                ImGui::SetWindowSize({ main_window.width - utility_window_size.x, main_window.height - inspector_window_size.y });
                if (ImGui::TreeNodeEx("Shaders", ImGuiTreeNodeFlags_None, "Shaders")) {
                    int is = 0;
                    for (auto& s : shaders)
                    {
                        if (ImGui::TreeNode(s.first)) {
                            for (auto& st : s.second.source) {
                                ImGui::LabelText("Shader type", "%s", cs::shader_type_to_string(st.first));

                                char label[15] = "";
                                snprintf(label, 15, "Source %d", ++is);
                                if (ImGui::TreeNode(st.second.path)) {
                                    ImGui::InputTextMultiline(label, st.second.src, strlen(st.second.src));
                                    ImGui::TreePop();
                                }
                            }

                            ImGui::TreePop();
                            ImGui::Separator();
                        }
                    }
                    ImGui::TreePop();
                }
                ImGui::Separator();
                if (ImGui::TreeNodeEx("Meshes", ImGuiTreeNodeFlags_None, "Meshes")) {
                    for (auto& s : meshes)
                    {
                        if (ImGui::TreeNode(s.first)) {
                            ImGui::LabelText("", "# Vertices %d", s.second.vertices.size());
                            ImGui::LabelText("", "# Indices  %d", s.second.indices.size());

                            for (auto& vb : s.second.vertex_array.get_vertex_buffers()) {
                                ImGui::LabelText("", "Vertex buffer Layout");
                                for (auto e : vb->get_layout().get_elements()) {
                                    ImGui::LabelText("", "%s\t%s", shader_data_type_to_string(e.type), e.name);
                                    ImGui::SameLine();
                                    ImGui::Checkbox("Normalized", &(e.normalized));
                                }
                            }

                            ImGui::TreePop();
                            ImGui::Separator();
                        }
                    }
                    ImGui::TreePop();
                }
                ImGui::Separator();
                if (ImGui::TreeNodeEx("Textures", ImGuiTreeNodeFlags_None, "Textures")) {
                    auto i = 0;
                    for (auto& t : textures)
                    {
                        if (i && (i % 5) < 4) ImGui::SameLine();
                        ImGui::Image((void*)(intptr_t)t.second.id_, ImVec2(80, 80), { 0, 0 }, { 1, -1 });
                        if (ImGui::IsItemHovered()) {
                            ImGui::BeginTooltip();
                            ImGui::LabelText("", "GL Texture id: %d", t.second.id_);
                            ImGui::LabelText("", "Size: %.0fx%.0f", t.second.size[0], t.second.size[1]);
                            ImGui::EndTooltip();
                        }
                        ++i;
                    }
                    ImGui::TreePop();
                }

                ImGui::End();
            }
            gui.render();
        }
#endif
        cs::input_update();
        glfwMakeContextCurrent(main_window.handle);
        glfwSwapBuffers(main_window.handle);
    }

    return 0;
}