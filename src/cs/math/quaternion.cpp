#include "quaternion.hpp"

namespace cs {
    quat quat::normalize() {
        quat r = *this;
        const float n = 1.0f/sqrt(x*x+y*y+z*z+w*w);
        r.x *= n; r.y *= n; r.z *= n; r.w *= n;
        return r;
    }
    
    mat4 quat::to_mat4()
    {
        mat4 r(1.0f);

        r[0][0] = 1 -   2 * y * y - 2 * z * z;
        r[0][1] =       2 * x * y + 2 * w * z;
        r[0][2] =       2 * x * z - 2 * w * y;
        r[1][0] =       2 * x * y - 2 * w * z;
        r[1][1] = 1 -   2 * x * x - 2 * z * z;
        r[1][2] =       2 * y * z + 2 * w * x;
        r[2][0] =       2 * x * z + 2 * w * y;
        r[2][1] =       2 * y * z - 2 * w * x;
        r[2][2] = 1 -   2 * x * x - 2 * y * y;

        return r;
    }

    void quat::print()
    {
        fprintf(stdout, "quat(%+3.4f, %+3.4f, %+3.4f, %+3.4f)\n", w, x, y, z);
    }

    cs::vec3 quat::to_euler_angles() {
        cs::vec3 angles;

        // roll (x-axis rotation)
        float sinr_cosp = 2 * (w * x + y * z);
        float cosr_cosp = 1 - 2 * (x * x + y * y);
        angles[0] = std::atan2(sinr_cosp, cosr_cosp);

        // pitch (y-axis rotation)
        double sinp = 2 * (w * y - z * x);
        if (std::abs(sinp) >= 1)
            angles[1] = std::copysign(M_PI / 2, sinp); // use 90 degrees if out of range
        else
            angles[1] = std::asin(sinp);

        // yaw (z-axis rotation)
        double siny_cosp = 2 * (w * z + x * y);
        double cosy_cosp = 1 - 2 * (y * y + z * z);
        angles[2] = std::atan2(siny_cosp, cosy_cosp);

        return angles;
    }

    /* TODO: Prob wrong?!? */    
    quat from_euler_angles(vec3 euler_angles, quat_to_euler_order order)
    {   
        float yaw{};
        float pitch{};
        float roll{};

        if(order == XYZ)
        {
            yaw = euler_angles[0];
            pitch = euler_angles[1];
            roll = euler_angles[2];
        }else if(order == ZYX)
        {
            yaw = euler_angles[2];
            pitch = euler_angles[1];
            roll = euler_angles[0];
        }else //YZX
        {
            yaw = euler_angles[1];
            pitch = euler_angles[2];
            roll = euler_angles[0];
        }

        float cy = cos(yaw * 0.5);
        float sy = sin(yaw * 0.5);
        float cp = cos(pitch * 0.5);
        float sp = sin(pitch * 0.5);
        float cr = cos(roll * 0.5);
        float sr = sin(roll * 0.5);

        quat q;
        q.w = cy * cp * cr + sy * sp * sr;
        q.x = cy * cp * sr - sy * sp * cr;
        q.y = sy * cp * sr + cy * sp * cr;
        q.z = sy * cp * cr - cy * sp * sr;

        return q;
    }
}