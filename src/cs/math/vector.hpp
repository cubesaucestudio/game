#pragma once

/* For printing TODO: Make a debugging logging class */
#include <cstdio>
/* For types */
#include <cstdint>
/* Self evident I'd say*/
#ifdef WIN32
#include <corecrt_math_defines.h>
#endif

#include <cmath>
/* For constructors */
#include <vector>
#include <initializer_list>

#include <type_traits>

namespace cs {
    template<typename data_type, uint32_t length>
    struct vec {
        data_type data[length]{};

        vec() : vec<data_type, length>(data_type(0)) {}
        
        vec(data_type v) { for(auto i = 0; i < length; ++i) data[i] = v; }

        vec(std::initializer_list<data_type> vs) {
            std::vector<data_type> list_v(vs);
            if(vs.size() < length)          for(auto i = 0; i < vs.size(); ++i) data[i] = list_v[i];
            else if(vs.size() >= length)    for(auto i = 0; i < length; ++i) data[i] = list_v[i];            
        }

        template<uint32_t len>
        vec(vec<data_type, len> o)
        {
            if(len < length)        for(auto i = 0; i < len; i++)       data[i] = o[i];
            else if(len >= length)  for(auto i = 0; i < length; i++)    data[i] = o[i];
        }

        template<uint32_t len>
        vec(vec<data_type, len> o, std::initializer_list<data_type> vs)
        {
            std::vector<data_type> list_v(vs);
            if(len < length)
            {
                for(auto i = 0; i < len; i++)       data[i] = o[i];
                for(auto i = 0; i < vs.size(); i++) if(len + i < length) data[len + i] = list_v[i];  
            }else if(len >= length)  for(auto i = 0; i < length; i++)    data[i] = o[i];
        }

        data_type& operator[](uint32_t index) { return data[index % length]; }

        void print(const char* precursor = "", const char* postcursor = "\n") {
            fprintf(stdout, "%svec%d(", precursor, length);
            for(auto i = 0; i < length; ++i)
            {
                if      (std::is_same<data_type, float>::value)     fprintf(stdout, "%+3.3f%s", float(data[i]), (i == length - 1) ? ")" : ", ");
                else if (std::is_same<data_type, double>::value)    fprintf(stdout, "%+3.3f%s", double(data[i]), (i == length - 1) ? ")" : ", ");
                else if (std::is_same<data_type, bool>::value)      fprintf(stdout,     "%s%s", bool(data[i]) ? "true" : "false", (i == length - 1) ? ")" : ", ");
                else                                                fprintf(stdout,    "%6d%s", int(data[i]), (i == length - 1) ? ")" : ", ");
            }

            fprintf(stdout, "%s", postcursor);
        }

        /*
            Perhaps it'd be better to check for length? (performance wise?)
        */
        void operator +=(const data_type &b)    { for(auto i = 0; i < length; ++i) data[i] += b; }
        void operator -=(const data_type &b)    { for(auto i = 0; i < length; ++i) data[i] -= b; }
        void operator *=(const data_type &b)    { for(auto i = 0; i < length; ++i) data[i] *= b; }
        void operator /=(const data_type &b)    { for(auto i = 0; i < length; ++i) data[i] /= b; }
        
        vec operator +(const data_type &b) { vec<data_type, length> r = *this; r += b; return r; }
        vec operator -(const data_type &b) { vec<data_type, length> r = *this; r -= b; return r; }
        vec operator *(const data_type &b) { vec<data_type, length> r = *this; r *= b; return r; }
        vec operator /(const data_type &b) { vec<data_type, length> r = *this; r /= b; return r; }
        
        void operator +=(const vec<data_type, length> &b)   { vec<data_type, length> bv(b); for(auto i = 0; i < length; ++i) data[i] += bv[i]; }
        void operator -=(const vec<data_type, length> &b)   { vec<data_type, length> bv(b); for(auto i = 0; i < length; ++i) data[i] -= bv[i]; }
        void operator *=(const vec<data_type, length> &b)   { vec<data_type, length> bv(b); for(auto i = 0; i < length; ++i) data[i] *= bv[i]; }
        void operator /=(const vec<data_type, length> &b)   { vec<data_type, length> bv(b); for(auto i = 0; i < length; ++i) data[i] /= bv[i]; }

        vec operator +(const vec<data_type, length> &b)    { vec<data_type, length> r = *this; r += b; return r; }
        vec operator -(const vec<data_type, length> &b)    { vec<data_type, length> r = *this; r -= b; return r; }
        vec operator *(const vec<data_type, length> &b)    { vec<data_type, length> r = *this; r *= b; return r; }
        vec operator /(const vec<data_type, length> &b)    { vec<data_type, length> r = *this; r /= b; return r; }

        vec<bool, length> operator ==(const vec<data_type, length> &b)   { vec<bool, length> r(false); vec<data_type, length> bv(b);  for(auto i = 0; i < length; ++i) r[i] = data[i] == bv[i]; return r; }
        vec<bool, length> operator !=(const vec<data_type, length> &b)   { vec<bool, length> r(false); vec<data_type, length> bv(b);  for(auto i = 0; i < length; ++i) r[i] = data[i] != bv[i]; return r; }

        float magnitude() {
            double sum = 0.0f;
            for(auto i = 0; i < length; ++i)
                sum += data[i] * data[i];

            return sqrt(sum);
        }

        vec reflect(vec normal) {
            return *this - (normal * (dot(normal) * 2));
        }

        vec normalize() {
            return (*this) / magnitude();
        }

        float dot(const vec &b) {
            auto bv(b);
            double r = 0;
            for(auto i = 0; i < length; ++i) r += data[i] * bv[i];
            return r;
        }

        vec max(vec b) {
            vec r = *this;
            for(auto i = 0; i < length; ++i) r[i] = r[i] > b[i] ? r[i] : b[i];
            return r;
        }

        data_type max() {
            data_type m = data[0];
            for(auto i = 1; i < length; ++i) if(data[i] > m) m = data[i];
            return m;
        }
    };

}