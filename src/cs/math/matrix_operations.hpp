#pragma once

#include "vector.hpp"
#include "matrix.hpp"

namespace cs {

    typedef mat<float, 2, 2> mat2;
    typedef mat<float, 3, 3> mat3;
    typedef mat<float, 4, 4> mat4;

    template<typename data_type,  uint32_t cols, uint32_t rows>
    cs::vec<data_type, cols> operator*( const cs::mat<data_type, cols, rows> &a,
                                        const cs::vec<data_type, cols> &b) 
    {
        cs::mat<data_type, cols, rows> am(a);
        cs::mat<data_type, 1, cols> bm({b});
        cs::mat<data_type, 1, cols> cm(0);

        for(auto i = 0u; i < rows; i++) 
        {
            for(auto j = 0u; j < cols; j++) 
            {
                cm[j][i] += am[j][i] * bm[0][j];
            }
            
        }
        
        return cm[0];
    }

    template<uint32_t n>
    float determinant(cs::mat<float, n, n> m) {
        float det = 0;
        for(auto i = 0; i < n; i++)
        {
            auto s = m.submatrix(i, 0);                
            det += ((i) % 2 ? -1 : 1) * m[i][0] * determinant<n-1>(s);
        }

        return det;   
    }

    template<>
    inline float determinant<1>(cs::mat<float, 1, 1> m) {
        return m[0][0];
    }

    template<typename mat>
    mat inverse(mat m) {
        auto d = determinant(m);
        if(!d) return m;
        return m / d;
    }

    cs::mat4 translate(cs::vec3 position);
    cs::mat4 scale(cs::vec3 scale);

    cs::mat4 orthographic(float left, float right, float bottom, float top, float zNear, float zFar);
    cs::mat4 perspective(float fovy, float aspect, float zNear, float zFar);
    cs::mat4 look_at(cs::vec3 position, cs::vec3 target, cs::vec3 up);

    cs::vec3 decompose_translation(cs::mat4 m);
    cs::vec3 decompose_scale(cs::mat4 m);
    cs::mat4 decompose_rotation(cs::mat4 m);
}