#pragma once

#include "vector.hpp"

namespace cs {
    typedef vec<float, 2> vec2;
    typedef vec<float, 3> vec3;
    typedef vec<float, 4> vec4;

    template<typename data_type>
    vec<data_type, 3> cross(const vec<data_type, 3> &a, const vec<data_type, 3> &b) {
        auto av = vec<data_type, 3>(a);
        auto bv = vec<data_type, 3>(b);

        return {
            av[1] * bv[2] - av[2] * bv[1],
            av[2] * bv[0] - av[0] * bv[2], 
            av[0] * bv[1] - av[1] * bv[0]};
    }
}