#pragma once

#include "vector_operations.hpp"
#include "matrix_operations.hpp"

namespace cs {
    struct quat {
        float x, y, z, w;
        quat() :w(0), x(0), y(0), z(0) {}

        quat normalize();
        cs::mat4 to_mat4();

        cs::vec3 to_euler_angles();

        void print();
    };

    enum quat_to_euler_order{
        XYZ, ZYX, YZX
    };

    quat from_euler_angles(vec3 euler_angles, quat_to_euler_order order = ZYX);
}