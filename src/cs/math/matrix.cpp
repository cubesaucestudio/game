#include "vector_operations.hpp"
#include "matrix_operations.hpp"

namespace cs {
    cs::mat4 translate(cs::vec3 position)
    {
        cs::mat4 r(1.0f);
        for(int p = 0; p < 3; p++)
            r[3][p] = position[p];
        return r;
    }

    cs::mat4 scale(cs::vec3 scale)
    {
        cs::mat4 r(1.0f);
        for(int s = 0; s < 3; s++)
            r[s][s] = scale[s];
        return r;
    }

    cs::vec3 decompose_translation(cs::mat4 m) {
        auto s = cs::vec3({
            m[3][0],
            m[3][1],
            m[3][2]
        });
        return s;
    }

    cs::vec3 decompose_scale(cs::mat4 m) {
        return {
            cs::vec3({m[0][0], m[0][1], m[0][2]}).magnitude(),
            cs::vec3({m[1][0], m[1][1], m[1][2]}).magnitude(),
            cs::vec3({m[2][0], m[2][1], m[2][2]}).magnitude()
        };
    }

    cs::mat4 decompose_rotation(cs::mat4 m) {
        auto r = m;
        auto s = decompose_scale(m);

        for(auto i = 0; i < 3; i++)
        {
            r[0][i] /= s[i];
            r[1][i] /= s[i];
            r[2][i] /= s[i];
            r[3][i]  =   0;
        }

        r[3][3] = 1;
        return r;
    }

    mat4 perspective(float fovy, float aspect, float zNear, float zFar) {
        const float ar = aspect;
        const float zRange = zNear - zFar;
        const float tanHalfFOV = tanf((M_PI / 180.0f) * (fovy / 2.0));

        mat4 m(0);

        m[0][0] = 1.0f / (tanHalfFOV * ar);
        m[1][1] = 1.0f / tanHalfFOV;
        m[2][2] = -(-zNear - zFar) / zRange;
        m[2][3] = -1.0f;
        m[3][2] = 2.0f * zFar * zNear / zRange;

        return m;
    }

    cs::mat4 orthographic(
        float left, float right, 
        float bottom, float top, 
        float zNear, float zFar) {
        mat4 m(1);

        m[0][0] =  float(2) / (right - left);
        m[1][1] =  float(2) / (top - bottom);
        m[2][2] = -float(2) / (zFar - zNear);
        m[3][0] = -(right + left) / (right - left);
        m[3][1] = -(top + bottom) / (top - bottom);
        m[3][2] = -(zFar + zNear) / (zFar - zNear);

        return m;
    }

    mat4 look_at(vec3 eye, vec3 target, vec3 up) {
        vec3  f = (target - eye).normalize();
        vec3  u = up.normalize();
        vec3  s = cross(f, u).normalize();
        u = cross(s, f);

        mat4 m(1);
        m[0][0] = s[0];
        m[1][0] = s[1];
        m[2][0] = s[2];
        m[0][1] = u[0];
        m[1][1] = u[1];
        m[2][1] = u[2];
        m[0][2] =-f[0];
        m[1][2] =-f[1];
        m[2][2] =-f[2];
        m[3][0] =-s.dot(eye);
        m[3][1] =-u.dot(eye);
        m[3][2] = f.dot( eye);

        return m;
    }
}