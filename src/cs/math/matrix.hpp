#pragma once

#include "vector.hpp"

namespace cs {
    /* Column major matrices */
    template<typename data_type, uint32_t cols, uint32_t rows>
    struct mat {
        cs::vec<data_type, rows> data[cols];

        mat() : mat<data_type, cols, rows>(data_type(0)) {}
        mat(data_type v) {
            if(rows == cols)
                for(auto i = 0; i < cols; i++)
                    data[i][i] = v;
            else
                for(auto i = 0; i < cols; i++)
                    for(auto j = 0; j < rows; j++)
                        data[i][j] = v;
        }

        mat(std::initializer_list<cs::vec<data_type, rows>> vs) {
            std::vector<cs::vec<data_type, rows>> list_v(vs);
            if(vs.size() < cols)          for(auto i = 0; i < vs.size(); ++i)   data[i] = list_v[i];
            else if(vs.size() >= cols)    for(auto i = 0; i < cols; ++i)        data[i] = list_v[i];            
        }

        cs::vec<data_type, rows>& operator[](uint32_t index) { return data[index % cols]; }

        void print(const char* precursor = "", const char* postcursor = "\n") {
            if(cols == rows)    fprintf(stdout, "%svec%d(\n", precursor, cols);
            else                fprintf(stdout, "%svec%dx%d(\n", precursor, cols, rows);
            for(auto i = 0; i < cols; ++i)
                data[i].print(" ");
            fprintf(stdout, ")%s", postcursor);
        }

        void operator +=(const data_type &b)    { for(auto i = 0; i < cols; ++i) data[i] += b; }
        void operator -=(const data_type &b)    { for(auto i = 0; i < cols; ++i) data[i] -= b; }
        void operator *=(const data_type &b)    { for(auto i = 0; i < cols; ++i) data[i] *= b; }
        void operator /=(const data_type &b)    { for(auto i = 0; i < cols; ++i) data[i] /= b; }
        
        mat operator +(const data_type &b) { mat<data_type, cols, rows> r = *this; r += b; return r; }
        mat operator -(const data_type &b) { mat<data_type, cols, rows> r = *this; r -= b; return r; }
        mat operator *(const data_type &b) { mat<data_type, cols, rows> r = *this; r *= b; return r; }
        mat operator /(const data_type &b) { mat<data_type, cols, rows> r = *this; r /= b; return r; }

        void operator +=(const mat<data_type, cols, rows> &b)   { mat<data_type, cols, rows> bv(b); for(auto i = 0; i < cols; ++i) data[i] += bv[i]; }
        void operator -=(const mat<data_type, cols, rows> &b)   { mat<data_type, cols, rows> bv(b); for(auto i = 0; i < cols; ++i) data[i] -= bv[i]; }

        mat operator +(const mat<data_type, cols, rows> &b)     { mat<data_type, cols, rows> r = *this; r += b; return r; }
        mat operator -(const mat<data_type, cols, rows> &b)     { mat<data_type, cols, rows> r = *this; r -= b; return r; }
        mat operator *(const mat<data_type, cols, rows> &b)     { mat<data_type, cols, rows> r = *this; return r.dot(b); }

        mat<data_type, rows, cols> transpose() {
            mat<data_type, rows, cols> r;

            for(auto i = 0; i < cols; ++i)
                for(auto j = 0; j < rows; ++j)
                    r[j][i] = data[i][j];

            return r;
        }

        template<uint32_t bcols>
        mat<data_type, bcols, rows> dot(const mat<data_type, bcols, cols> &b) {
            mat<data_type, cols, rows> am(*this);
            mat<data_type, bcols, cols> bm(b);
            mat<data_type, bcols,rows> cm(0);

            for(auto i = 0; i < rows; i++)
            {
                for(auto j = 0; j < bcols; j++)
                {
                    for(auto k = 0; k < cols; k++)
                    {
                        cm[j][i] += am[k][i] * bm[j][k];                        
                    }
                }
            }

            return cm;
        }

        auto submatrix(uint32_t r, uint32_t c) {
            const uint32_t crows = rows - 1;
            const uint32_t ccols = cols - 1;

            mat<data_type, crows, ccols> sm{};
            for(auto i = 0u; i < rows; i++) {
                for(auto j = 0u; j < cols; j++) {
                    if      (i < r && j < c) sm[ i ][ j ] = data[i][j];
                    else if (i > r && j < c) sm[i-1][ j ] = data[i][j];
                    else if (i < r && j > c) sm[ i ][j-1] = data[i][j];
                    else if (i > r && j > c) sm[i-1][j-1] = data[i][j];
                }
            }
            
            return sm;
        }
    };
}