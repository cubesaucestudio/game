#pragma once

#include <cs/window.hpp>

#include <cs/imgui/imgui.h>
#include <cs/imgui/imgui_impl_opengl3.h>
#include <cs/imgui/imgui_impl_glfw.h>

namespace cs {

    struct gui {
        cs::window *window;

        gui(cs::window &_window, const char* glsl_version);
        ~gui();

        void begin();
        void render();
    };
}