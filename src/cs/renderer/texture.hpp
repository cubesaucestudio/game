#pragma once

#include <cstdint>
#include <cs/math/vector_operations.hpp>

namespace cs {
    struct texture {
        uint32_t id_ = 0;
        cs::vec2 size{};

        texture() {}
        texture(const char* path);
        texture(cs::vec2 size);

        void bind();
        void unbind();
    };

    void load_texture(const char* path, const texture& texture);
}