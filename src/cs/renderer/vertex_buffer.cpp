#include "vertex_buffer.hpp"

#include <GL/glew.h>

#include <cstdio>

namespace cs {
    	static uint32_t shader_data_type_size(const shader_data_type type)
	{
		switch (type)
		{
		case shader_data_type::float1:    return 4;
		case shader_data_type::float2:   return 4 * 2;
		case shader_data_type::float3:   return 4 * 3;
		case shader_data_type::float4:   return 4 * 4;
		case shader_data_type::mat3:     return 4 * 3 * 3;
		case shader_data_type::mat4:     return 4 * 4 * 4;
		case shader_data_type::int1:      return 4;
		case shader_data_type::int2:     return 4 * 2;
		case shader_data_type::int3:     return 4 * 3;
		case shader_data_type::int4:     return 4 * 4;
		case shader_data_type::bool1:     return 1;
		default: return 0;
		}
	}

	buffer_element::buffer_element(shader_data_type type, const char* name, const bool normalized)
		: name(name), type(type), size(shader_data_type_size(type)), offset(0), normalized(normalized)
	{
		
	}
	
	const char* shader_data_type_to_string(shader_data_type d){
		switch (d)
		{
		case shader_data_type::float1: return "float1";
		case shader_data_type::float2: return "float2";
		case shader_data_type::float3: return "float3";
		case shader_data_type::float4: return "float4";
		case shader_data_type::mat3: return "mat3";
		case shader_data_type::mat4: return "mat4";
		case shader_data_type::int1: return "int1";
		case shader_data_type::int2: return "int2";
		case shader_data_type::int3: return "int3";
		case shader_data_type::int4: return "int4";
		case shader_data_type::bool1: return "bool1";		
		default: return "none";
		}
	}

	uint32_t buffer_element::get_component_count() const
	{
		switch (type)
		{
		case shader_data_type::float1: return 1;
		case shader_data_type::float2: return 2;
		case shader_data_type::float3: return 3;
		case shader_data_type::float4: return 4;
		case shader_data_type::mat3: return 3 * 3;
		case shader_data_type::mat4: return 4 * 4;
		case shader_data_type::int1: return 1;
		case shader_data_type::int2: return 2;
		case shader_data_type::int3: return 3;
		case shader_data_type::int4: return 4;
		case shader_data_type::bool1: return 1;
		default: return 0;
		}
	}

	void buffer_layout::add_element(buffer_element be)
	{
		elements_.push_back(be);
		calculate_offsets_and_stride();
	}

	void buffer_layout::calculate_offsets_and_stride()
	{
		size_t offset = 0;
		stride_ = 0;
		for (auto& element : elements_)
		{
			element.offset = offset;
			offset += element.size;
			stride_ += element.size;
		}
	}

	vertex_buffer::vertex_buffer(float* vertices, uint32_t size)
	{
		glCreateBuffers(1, &id_);
		glBindBuffer(GL_ARRAY_BUFFER, id_);
		glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_STATIC_DRAW);
	}

	vertex_buffer::~vertex_buffer()
	{
		// printf("Deleting vbo %d\n", id_);
		glDeleteBuffers(1, &id_);
	}

	void vertex_buffer::bind() const
	{
		glBindBuffer(GL_ARRAY_BUFFER, id_);
	}

	void vertex_buffer::unbind()
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void vertex_buffer::set_layout(const buffer_layout& layout)
	{
		layout_ = layout;
	}

	buffer_layout vertex_buffer::get_layout() const
	{
		return layout_;
	}

	index_buffer::index_buffer(uint32_t* indices, const uint32_t count)
		:count_(count)
	{
		glCreateBuffers(1, &id_);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id_);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(uint32_t), indices, GL_STATIC_DRAW);
	}

	index_buffer::~index_buffer()
	{
		// printf("Deleting ibo %d\n", id_);
		glDeleteBuffers(1, &id_);
	}

	void index_buffer::bind()
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id_);
	}

	void index_buffer::unbind()
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
}