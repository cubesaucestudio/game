#pragma once

#include <cstdint>
#include "texture.hpp"

/*
    TODO: Look into making a single buffer class, 
            that expands as needed
*/
namespace cs {
    struct framebuffer{
        uint32_t id_;

        framebuffer();

        void bind();
        void unbind();

        uint32_t check_status();

        void texture_2d(texture texture);
    };


    struct renderbuffer {
        uint32_t id_;

        renderbuffer();

        void bind();
        void storage(cs::vec2 size);
        void framebuffer();
    };
}