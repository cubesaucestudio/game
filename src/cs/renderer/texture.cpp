#include "texture.hpp"
#include <GL/glew.h>

#include <cs/utility/stb_image.h>
#include <cs/utility/log.hpp>
namespace cs {
    texture::texture(const char* path) {
        glGenTextures(1, &id_);
        glBindTexture(GL_TEXTURE_2D, id_); // all upcoming GL_TEXTURE_2D operations now have effect on this texture object
        // set the texture wrapping parameters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// set texture wrapping to GL_REPEAT (default wrapping method)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        // set texture filtering parameters
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // load image, create texture and generate mipmaps
        int width, height, nrChannels;
        // The FileSystem::getPath(...) is part of the GitHub repository so we can find files on any IDE/platform; replace it with your own image path.
        unsigned char* data = stbi_load(path, &width, &height, &nrChannels, 0);
       
        if (data)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);

            size = { float(width), float(height) };
            cs::log::texture("Sucessfuly loaded \"%s\" as [%03d] of size %.0fx%.0f", path, id_, size[0], size[1]);
        }
        else
        {
            cs::log::texture("Failed to load \"%s\"", path);
        }
        stbi_image_free(data);
    }

    texture::texture(cs::vec2 size) {
        glGenTextures(1, &id_);
        glBindTexture(GL_TEXTURE_2D, id_);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, size[0], size[1], 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    void texture::bind() { glBindTexture(GL_TEXTURE_2D, id_); }
    void texture::unbind() { glBindTexture(GL_TEXTURE_2D, 0); }


    void load_texture(const char* path, const texture& texture) {
        
    }
}