#include "renderer.hpp"

#include <GL/glew.h>

namespace cs {
	namespace renderer {
		void wireframe(bool on) {
			if(on)
                glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
			else
                glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
		}

		void draw_indexed(unsigned mode, cs::shader* shader, cs::mat4 view_projection, cs::vertex_array* vertex_array, cs::mat4 model) {
			shader->upload_uniform_mat4("u_view_projection", view_projection);
			shader->upload_uniform_mat4("u_model", model);

			vertex_array->bind();
			glDrawElements(mode, vertex_array->get_index_buffer()->get_count(), GL_UNSIGNED_INT, nullptr);
			vertex_array->unbind();
		}
	}
}