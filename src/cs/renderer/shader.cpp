#include "shader.hpp"
#include <GL/glew.h>

#include <cs/utility/file.hpp>
#ifndef NDEBUG
#include <cs/utility/log.hpp>
#endif

namespace cs {
    unsigned shader_type_to_gl_shader_type(shader_source_type type)
    {
        switch (type)
        {
        case vertex:	return GL_VERTEX_SHADER;
        case geometry:	return GL_GEOMETRY_SHADER;
        case pixel:		return GL_FRAGMENT_SHADER;
        default:		return 0;
        }
    }

    const char *shader_type_to_string(shader_source_type type) {
        switch (type)
        {
        case vertex:	return "VERTEX_SHADER";
        case geometry:	return "GEOMETRY_SHADER";
        case pixel:		return "FRAGMENT_SHADER";
        default:		return 0;
        }
    }

    shader::shader() {}
    shader::shader(const shader& other) {
        id      = other.id;
        source  = other.source;
    }
	
    void shader::bind() { glUseProgram(id); }
    void shader::unbind() { glUseProgram(0); }
	
    void shader::add_shader_src(const char* path, shader_source_type type) {
        source[type].path = path;
        cs::file file(path);
        file.read(&source[type].src);
    }

    bool shader::compile() {
        id = glCreateProgram();

        for(auto shader_source : source)
        {
            auto& src = shader_source.second.src;
            auto& sid = shader_source.second.id;
            auto& stype = shader_source.first;

            sid = glCreateShader(shader_type_to_gl_shader_type(stype));

            char* a = &src[0];
            const char* const* s = &a;
            glShaderSource(sid, 1, s, nullptr);
            glCompileShader(sid);
            int compile_success_;
            glGetShaderiv(sid, GL_COMPILE_STATUS, &compile_success_);
            if (!compile_success_)
            {
#ifndef NDEBUG
                GLint length = 0;
                glGetShaderiv(shader_source.second.id, GL_INFO_LOG_LENGTH, &length);

                GLchar* buff = new GLchar[length];
                glGetShaderInfoLog(shader_source.second.id, length, &length, buff);
                printf("%s\n", buff);
                cs::log::shader("Compilation failure!\n%s", buff);
#endif
                glDeleteShader(sid); // Don't leak the shader.
                return false;
            }

            glAttachShader(id, sid);
            cs::log::shader("Succesfully compiled \"%s\"", shader_source.second.path);
        }

        glLinkProgram(id);
        return true;
    }

    void shader::upload_uniform_int1(const char* name, const int& x) const
    {
        const GLint location = glGetUniformLocation(id, name);
        glUniform1i(location, x);
    }

    void shader::upload_uniform_float1(const char* name, const float& x) const
    {
        const GLint location = glGetUniformLocation(id, name);
        glUniform1f(location, x);
    }

    void shader::upload_uniform_float3(const char* name, const float& x, const float& y, const float& z) const
    {
        const GLint location = glGetUniformLocation(id, name);
        glUniform3f(location, x, y, z);
    }

    void shader::upload_uniform_float3(const char* name, cs::vec3 v) const
    {
        const GLint location = glGetUniformLocation(id, name);
        glUniform3f(location, v[0], v[1], v[2]);
    }

    void shader::upload_uniform_mat4(const char* name, cs::mat4 v, bool transpose) const
    {
        const GLint location = glGetUniformLocation(id, name);
        glUniformMatrix4fv(location, 1, transpose, &v[0][0]);
    }
}