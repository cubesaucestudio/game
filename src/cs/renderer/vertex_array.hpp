#pragma once
#include "vertex_buffer.hpp"

#include <vector>
#include <cstdint>

namespace cs {
	class vertex_array final
	{
		uint32_t id_{};
		uint32_t vertex_buffer_index_ = 0;
		std::vector<vertex_buffer*> vertex_buffers_;
		index_buffer* index_buffer_{};

	public:
		vertex_array();
		~vertex_array();

		void bind();
		void unbind();

		void add_vertex_buffer(vertex_buffer* vertex_buffer);
		void set_index_buffer(index_buffer* index_buffer);

		const std::vector<vertex_buffer*>& get_vertex_buffers() const;
		index_buffer* get_index_buffer() const;
	private:
	};
}