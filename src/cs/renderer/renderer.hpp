#pragma once

#include "shader.hpp"
#include "vertex_array.hpp"

#include <cs/math/matrix.hpp>

namespace cs {
    namespace renderer{
      void wireframe(bool on = true);
		  void draw_indexed(unsigned mode, cs::shader* shader, cs::mat4 view_projection, cs::vertex_array* vertex_array, cs::mat4 model);
    }
}