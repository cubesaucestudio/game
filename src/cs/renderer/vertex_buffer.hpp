#pragma once

#include <vector>
#include <cstdint>

namespace cs {
	enum class shader_data_type
	{
		none = 0,
		float1, float2, float3, float4,
		mat3, mat4,
		int1, int2, int3, int4,
		bool1
	};

	const char* shader_data_type_to_string(shader_data_type d);

	struct buffer_element
	{
		const char* name;
		shader_data_type type;
		uint32_t size;
		size_t offset;
		bool normalized;

		buffer_element() = default;
		buffer_element(shader_data_type type, const char* name, bool normalized = false);

		uint32_t get_component_count() const;
	};

	class buffer_layout {
	private:
		std::vector<buffer_element> elements_;
		uint32_t stride_ = 0;
	public:
		buffer_layout() = default;

		buffer_layout(const std::initializer_list<buffer_element>& elements)
			:elements_(elements)
		{
			calculate_offsets_and_stride();
		}

		void add_element(buffer_element be);
		uint32_t get_stride() const { return stride_; }
		const std::vector<buffer_element> get_elements() const { return elements_; }

		std::vector<buffer_element>::iterator begin() { return elements_.begin(); }
		std::vector<buffer_element>::iterator end() { return elements_.end(); }
		std::vector<buffer_element>::const_iterator begin() const { return elements_.begin(); }
		std::vector<buffer_element>::const_iterator end() const { return elements_.end(); }
	private:
		void calculate_offsets_and_stride();
	};

	class vertex_buffer {
	private:
		uint32_t id_{};
		buffer_layout layout_;
	public:
		vertex_buffer(float* vertices, uint32_t size);
		~vertex_buffer();

		void bind() const;
		static void unbind();

		void set_layout(const buffer_layout& layout);
		buffer_layout get_layout() const;
	};

	class index_buffer {
	private:
		uint32_t id_{};
		uint32_t count_;
	public:
		index_buffer(uint32_t* indices, uint32_t count);
		~index_buffer();

		void bind();
		void unbind();

		uint32_t get_count() const { return count_; }
	};
}