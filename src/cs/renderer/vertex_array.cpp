#include "vertex_array.hpp"

#include <GL/glew.h>

#include <cstdio>

namespace cs {
	static GLenum shader_data_type_to_open_gl_base_type(const shader_data_type type)
	{
		switch (type)
		{
		case shader_data_type::float1:    return GL_FLOAT;
		case shader_data_type::float2:   return GL_FLOAT;
		case shader_data_type::float3:   return GL_FLOAT;
		case shader_data_type::float4:   return GL_FLOAT;
		case shader_data_type::mat3:     return GL_FLOAT;
		case shader_data_type::mat4:     return GL_FLOAT;
		case shader_data_type::int1:      return GL_INT;
		case shader_data_type::int2:     return GL_INT;
		case shader_data_type::int3:     return GL_INT;
		case shader_data_type::int4:     return GL_INT;
		case shader_data_type::bool1:     return GL_BOOL;
		default: return 0;
		}
	}

	vertex_array::vertex_array()
	{
		glCreateVertexArrays(1, &id_);
	}

	vertex_array::~vertex_array()
	{
		// printf("Deleting vao %d\n", id_);
		glDeleteVertexArrays(1, &id_);	
	}

	void vertex_array::bind()
	{
		glBindVertexArray(id_);
	}

	void vertex_array::unbind()
	{
		glBindVertexArray(0);
	}

	void vertex_array::add_vertex_buffer(vertex_buffer* vertex_buffer)
	{
		glBindVertexArray(id_);
		vertex_buffer->bind();

		const auto& layout = vertex_buffer->get_layout();
		for (const auto& element : layout)
		{
			glEnableVertexAttribArray(vertex_buffer_index_);
			if (element.type == shader_data_type::int4)
			{
				//std::cout << "Offset " << element.Offset << "\n";
				glVertexAttribIPointer(
					vertex_buffer_index_,
					element.get_component_count(),
					shader_data_type_to_open_gl_base_type(element.type),
					layout.get_stride(),
					reinterpret_cast<const void*>(element.offset)
				);
			}
			else
			{
				glVertexAttribPointer(vertex_buffer_index_,
					element.get_component_count(),
					shader_data_type_to_open_gl_base_type(element.type),
					element.normalized ? GL_TRUE : GL_FALSE,
					layout.get_stride(),
					reinterpret_cast<const void*>(element.offset));

			}
			vertex_buffer_index_++;
		}

		vertex_buffers_.push_back(vertex_buffer);	
	}

	void vertex_array::set_index_buffer(index_buffer* index_buffer)
	{
		glBindVertexArray(id_);
		index_buffer->bind();

		index_buffer_= index_buffer;
	}

	const std::vector<vertex_buffer*>& vertex_array::get_vertex_buffers() const
	{
		return vertex_buffers_;
	}

	index_buffer* vertex_array::get_index_buffer() const
	{
		return index_buffer_;
	}
}