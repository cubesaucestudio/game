#include "framebuffer.hpp"

#include <GL/glew.h>

namespace cs {
    framebuffer::framebuffer() {
        glGenFramebuffers(1, &id_);
    }

    void framebuffer::bind() { glBindFramebuffer(GL_FRAMEBUFFER, id_); }
    void framebuffer::unbind() { glBindFramebuffer(GL_FRAMEBUFFER, 0); }

    uint32_t framebuffer::check_status() { return glCheckFramebufferStatus(GL_FRAMEBUFFER); }

    void framebuffer::texture_2d(texture texture) { glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.id_, 0); }

    renderbuffer::renderbuffer() {
        glGenRenderbuffers(1, &id_);
    }

    void renderbuffer::bind() {
        glBindRenderbuffer(GL_RENDERBUFFER, id_);
    }

    void renderbuffer::storage(cs::vec2 size) {
        renderbuffer::bind();
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, size[0], size[1]); // use a single renderbuffer object for both a depth AND stencil buffer.
    }

    void renderbuffer::framebuffer() {
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, id_); // now actually attach it
    }
}