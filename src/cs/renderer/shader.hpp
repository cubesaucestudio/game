#pragma once

#include <map>
#include <cs/math/vector_operations.hpp>
#include <cs/math/matrix_operations.hpp>

namespace cs { 

    enum shader_source_type{
        vertex   = 0llu,
        geometry = 1llu, 
        fragment = 2llu,
        pixel    = 2llu
    };

    unsigned shader_type_to_gl_shader_type(shader_source_type type);
    const char *shader_type_to_string(shader_source_type type);

    struct shader_source{
        unsigned id;
        const char* path;
        char src[4096];
    };

    struct shader{
        unsigned id;
        std::map<shader_source_type, shader_source> source;
		
        shader();
        shader(const shader& other);

        ~shader(){
            //for(auto s : source)
            //    delete[] s.second.src;
        }

        void bind();
        void unbind();
        void add_shader_src(const char* path, shader_source_type type);

        bool compile();

		void upload_uniform_int1(const char* name, const int& x) const;

		void upload_uniform_float1(const char* name, const float& x) const;
		void upload_uniform_float3(const char* name, const float& x, const float& y, const float& z) const;

		void upload_uniform_float3(const char* name, cs::vec3 v) const;

		void upload_uniform_mat4(const char* name, cs::mat4 v, bool transpose = false) const;
    };
}
