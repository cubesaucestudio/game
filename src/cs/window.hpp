#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <functional>

namespace cs {
    struct window {
        GLFWwindow*     handle      = nullptr;
        int             width       = 0;
        int             height       = 0;

        window(int width = 1280, int height = 720, const char* title = "Window");
        ~window();

        void poll_events();

        bool should_close();
        void close();
        
        void update(float, float);
        void render();

        std::function<void(float, float)> on_update = [](float, float){};
        std::function<void()> on_render = [](){};
    };
}