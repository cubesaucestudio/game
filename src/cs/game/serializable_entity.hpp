#pragma once

#include <cs/math/vector.hpp>
#include <cs/math/matrix.hpp>
#include <cs/math/quaternion.hpp>

#include <vector>

namespace cs {
	namespace serializable {
		struct entity {
            uint32_t                id          = 0;
            uint32_t                parent_id   = 0;
            entity*                 parent      = nullptr;

            char                    name[72]    = "";
            std::vector<uint32_t>   children_id = {};
            std::vector<entity*>    children    = {};

            cs::vec3    position        = {};
            cs::quat    rotation        = {};
            cs::vec3    scale           = cs::vec3(1);

            entity() {
            
            }

            entity& add() {
                children.push_back(entity());
                auto& e = *(--children.end());
                e.id = id + children.size();
                e.parent_id = id;
                return e;
            }
		};

        struct scene {
            entity      root;

            entity& add() {
                auto& e = root.add();

                return e;
            }
        };
	}
}