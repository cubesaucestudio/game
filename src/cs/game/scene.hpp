#pragma once

#include "entity.hpp"

namespace cs {
    /*
        Scene contains some root entities, which in turn have their 
        own root entities (aka. children) and so on.

        Scene saving/loading should utilize that parent/child
        relationships, built in in entities and/or transforms.
    */

    struct scene {
        std::string name{};
        std::string path{};
        
        entity* root;
        camera* active_camera;

        scene(const char* name, const char* path = "") {
            this->name = name;
            this->path = path;

            root = new entity("Root", nullptr);
        }

        ~scene(){
            /* Idk, i need to destroy chidlren first */
        }

        void update(float dt, float time) {
            root->_update(dt, time);
        }

        void render() {
            root->_render(active_camera);
        }

        template<typename entity_type>
        entity_type* add(const char* name) {
            auto e = root->add<entity_type>(name);

            if(e->entity_type == camera_e)
            {
                auto ce = (cs::camera*) e;
                active_camera = ce;
            }

            return e;
        }

        template<typename entity_type>
        void remove(entity_type *e) {
            return root->remove(e);
        }

        void save() {
            cs::file cssf(path.c_str(), O_RDWR | O_CREAT);

            cssf.close();
        }

        void load() {
        
        }
    };
}