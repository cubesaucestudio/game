#pragma once

#include <functional>
#include <algorithm>
#include <string>

#include <GLFW/glfw3.h>

#include <cs/math/math>
#include <cs/utility/io.hpp>
#include <cs/renderer/renderer.hpp>

namespace cs {

    /* Constants*/
    cs::vec3 right{ 1, 0, 0 };
    cs::vec3 world_up{ 0, 0, 1 };

    struct entity;
    struct camera;

    struct transform {
        cs::entity* entity;
        
        cs::transform* parent;
        std::vector<cs::transform*> children;

        cs::vec3 position{};
        cs::quat rotation{};
        cs::vec3 scale = cs::vec3(1);

        transform(cs::entity* entity, cs::transform* parent)
            : entity(entity), parent(parent)
        {
            if (parent != nullptr) {
                if (std::find(parent->children.begin(), parent->children.end(), this) == parent->children.end()) {
                    parent->children.push_back(this);
                }
            }
        }

        cs::vec3 direction() {
            return cs::cross(world_up, right);
        }

        cs::mat4 local_to_parent() {
            return cs::translate(position) * rotation.to_mat4() * cs::scale(scale);
        };

        /* TODO -> implement inverse of matrix and vector */
        cs::mat4 parent_to_local() {
            return cs::mat4(1.0f);
        };

        cs::mat4 local_to_world() {
            if(parent) {
                return parent->local_to_world() * local_to_parent();
            } else {
                return local_to_parent();
            }
        }

        cs::mat4 world_to_local() {
            if(parent) {
                return parent_to_local() * parent->world_to_local();
            } else {
                return parent_to_local();
            }
        }
    };

    enum entity_type_e {
        entity_e    = 0,
        camera_e    = 1,
        light_e     = 2,
        object_e    = 3,
        player_e    = 4,
    };  //uint8_t
    
    struct entity {
        uint32_t        id;
        std::string     name = "entity";
        entity_type_e   entity_type;

        std::function<void(float, float)> update = [](float, float){};
        std::function<void(cs::camera*)> render = [](cs::camera*){};

        cs::transform *transform;

        entity(const char* name, entity* parent, entity_type_e entity_type = entity_e)
            :name(name), entity_type(entity_type)
        {
            if(parent)
            {
                transform = new cs::transform(this, parent->transform);
            }else
                transform = new cs::transform(this, nullptr);
        }

        ~entity() {
            destroy();
        }

        void destroy() {
            /*  All children entities are destroyed too 
                TODO:
                    - destruction before/after update/render begins
            */
        }

        void write_to_file(cs::file& file) {
            cs::log::info("Writing an entity [%d] %s", id, name.c_str());

            /* Every entity has this */
            file.write(&(id));
            file.write(&(entity_type));

            auto name_len = uint32_t(name.length());
            file.write(&name_len);
            file.write(name.c_str(), name.length());
            
            file.write(&(transform->position));
            file.write(&(transform->rotation));
            file.write(&(transform->scale));

            if (transform->parent) file.write(&(transform->parent->entity->id));
            auto num_children = transform->children.size();
            file.write(&(num_children));
            for (auto c : transform->children)
                file.write(&(c->entity->id));

            _write_to_file(file);
        }

        virtual void _write_to_file(cs::file& file) {}

        void _update(float dt, float time) {
            update(dt, time);

            for(auto c : transform->children)
                c->entity->_update(dt, time);
        }

        void _render(cs::camera* camera) {
            render(camera);

            for(auto c : transform->children)
                c->entity->_render(camera);
        }

        template<typename entity_type>
        entity_type* add(const char* name) {
            auto e = new entity_type(name, this);
            return e;
        }

        template<typename entity_type>
        void remove(entity_type *e) {
            auto i_e = std::find(transform->children.begin(), transform->children.end(), e->transform);
            if(i_e == transform->children.end())
            {
                for(auto ret : transform->children)
                    ret->entity->remove(e);
            } else {
                transform->children.erase(i_e);
                delete e;
            }
        }
    };

    enum camera_type_e{
        NONE_C          = 0,
        PERSPECTIVE     = 1,
        ORTHOGRAPHIC    = 2,
    }; //uint8_t

    struct camera : entity {
        camera_type_e camera_type;
        camera(const char* name, entity* parent, camera_type_e camera_type = NONE_C)
            :entity(name, parent, camera_e), camera_type(camera_type)
        {}

        void _write_to_file(cs::file& file) override {
            cs::log::info("\tCamera");
            file.write(&camera_type);
            _write_camera_entity_to_file(file);
        }

        virtual void _write_camera_entity_to_file(cs::file& file) {}

        virtual cs::mat4 view() {
            auto t = transform->local_to_world();
            auto inverse = cs::inverse(t);
            auto direction = cs::vec3({
                inverse[1][0],
                inverse[1][1],
                inverse[1][2],
                });

            auto world_pos = cs::decompose_translation(t);
            return cs::look_at(world_pos, world_pos + direction.normalize(), world_up);
        }

        virtual cs::mat4 projection() { return cs::mat4(1); }
        cs::mat4 view_projection() { return projection() * view(); }
    };

    struct perspective_camera : camera {
        float aspect_ratio = float(1280) / float(720);
        float fov = 60.0f;
        float z_near = 0.001f;
        float z_far = 1000.0f;

        void _write_camera_entity_to_file(cs::file& file) override {
            cs::log::info("\t\tPerspective");
            file.write(&aspect_ratio);
            file.write(&fov);
            file.write(&z_near);
            file.write(&z_far);
        }

        perspective_camera(const char* name, entity* parent) 
            :camera(name, parent, PERSPECTIVE)
        {
        }

        cs::mat4 projection() override {
            return cs::perspective(fov, aspect_ratio, z_near, z_far);
        }
    };

    struct orthographic_camera : camera {
        float left      = 0.0f;
        float right     = 1024.0f;
        float bottom    = 0.0f;
        float top       = 576.0f;
        float z_near    = -1;
        float z_far     =  1;

        void _write_camera_entity_to_file(cs::file& file) override {
            cs::log::info("\t\tOrthographic");
            file.write(&left);
            file.write(&right);
            file.write(&bottom);
            file.write(&top);
            file.write(&z_near);
            file.write(&z_far);
        }

        orthographic_camera(const char* name, entity* parent) 
            :camera(name, parent, ORTHOGRAPHIC)
        {}

        cs::mat4 projection() override {
            return cs::orthographic(left, right, bottom, top, z_near, z_far);
        }
    };

    enum light_type_e{
        NONE_L      = 0,
        DIRECTIONAL = 1,
        SPOT        = 2,
        POINT       = 3,
    };  //uint8_t
    
    struct light : entity {
        light_type_e light_type;

        light(const char* name, entity* parent, light_type_e light_type = NONE_L) 
            : entity(name, parent, light_e), light_type(light_type)
        {}
    };
    
    struct directional_light : light {
        directional_light(const char* name, entity* parent) 
            : light(name, parent, DIRECTIONAL)
        {}

        void _write_to_file(cs::file & file) override {
            cs::log::info("\tDirectional light");
        }
    };

    struct spot_light : light {
        spot_light(const char* name, entity* parent) 
            : light(name, parent, SPOT)
        {}

        void _write_to_file(cs::file& file) override {
            cs::log::info("\tSpot light");

        }
    };

    struct point_light : light {
        point_light(const char* name, entity* parent) 
            : light(name, parent, POINT)
        {}

        void _write_to_file(cs::file& file) override {
            cs::log::info("\tPoint light");
        }
    };

    struct object : entity {
        std::string mesh_str;
        cs::mesh    *mesh   = nullptr;
        std::string shader_str;
        cs::shader  *shader = nullptr;
        std::string albedo_str;
        cs::texture *albedo = nullptr;

        cs::vec3 color{0.3, 0.4, 0.7};
         
        void _write_to_file(cs::file& file) override {
            cs::log::info("\tObject");
        }

        object(const char* name, entity* parent)
            : entity(name, parent, object_e)
        {
            mesh = new cs::mesh();

            render = [this](cs::camera* camera){
                this->shader->bind();
                this->shader->upload_uniform_float3("u_color", color);
                this->shader->upload_uniform_mat4("u_view", camera->view());
                this->shader->upload_uniform_mat4("u_projection", camera->projection());
                this->shader->upload_uniform_float3("u_camera_position", camera->transform->position);
                
                uint32_t num_tex = 0;
                if (albedo != nullptr) {
                    glActiveTexture(GL_TEXTURE0 + num_tex);
                    albedo->bind();
                    this->shader->upload_uniform_int1("u_albedo", num_tex);
                }
                
                cs::renderer::draw_indexed(GL_TRIANGLES, this->shader, camera->view_projection(),
                    &(this->mesh->vertex_array), transform->local_to_world());

                this->shader->unbind();
            };
        }
    };

    struct player : entity {
        cs::object *object;
        
        player(const char* name, entity* parent) 
            :entity(name, parent, player_e)
        {
            update = [this](float dt, float time){
                
                float angular_speed = 1.5f;
                float speed = 25;
                float up_speed = 15;
            
                cs::vec2 off = cs::get_mouse_delta();
                float sensitivity = dt * 0.08;
                off *= sensitivity;

                cs::vec3 orientation_angles = this->transform->rotation.to_euler_angles();
                orientation_angles[2] += off[0];
                this->transform->rotation = cs::from_euler_angles(orientation_angles);

                auto dir    = cs::vec3(cs::decompose_rotation(this->transform->local_to_world()) * cs::vec4({ 0, 1, 0, 1 }));
                auto orient = dir * dt;
                auto right  = cs::cross(orient, world_up);
                
                if(cs::is_key_pressed(GLFW_KEY_W)) this->transform->position += orient * speed;
                if(cs::is_key_pressed(GLFW_KEY_S)) this->transform->position -= orient * speed;
                if(cs::is_key_pressed(GLFW_KEY_A)) this->transform->position -= right * speed;
                if(cs::is_key_pressed(GLFW_KEY_D)) this->transform->position += right * speed;

                if(cs::is_key_pressed(GLFW_KEY_SPACE))
                    if(cs::is_key_pressed(GLFW_KEY_LEFT_CONTROL))
                        this->transform->position += cs::vec3({0, 0,-dt}) * up_speed;
                    else
                        this->transform->position += cs::vec3({0, 0, dt}) * up_speed;
            };
        }
    };
}
