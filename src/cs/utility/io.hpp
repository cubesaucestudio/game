#pragma once

#include <cs/math/math>

namespace cs {
    void        press_key(uint32_t key);
    void        release_key(uint32_t key);
    uint32_t    is_key_pressed(uint32_t key);
    bool        is_key_released(uint32_t key);
    bool        is_key_held(uint32_t key);

    void        set_joystick_connected(bool conn);
    bool        is_joystick_connected();

    cs::vec2    get_joystick_axes();

    void        input_update();
    
    void        disable_mouse(bool disable);
    cs::vec2    get_mouse();
    cs::vec2    get_mouse_delta();
    void        set_mouse(cs::vec2 m);
    void        set_mouse_delta(cs::vec2 md);
}