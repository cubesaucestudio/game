#include <io.hpp>

#include <map>
#include <GLFW/glfw3.h>

namespace cs {
    std::map<uint32_t, uint32_t> key_press_map;
    std::map<uint32_t, bool> key_release_map;
    cs::vec2 mouse{};
    cs::vec2 mouse_delta{};

    bool mouse_disabled     = false;    
    bool joystick_connected = false;

    void        set_joystick_connected(bool conn) { joystick_connected = conn; }
    bool        is_joystick_connected() { return joystick_connected; }

    void input_update() {
        for (auto& k : key_press_map)
            if(k.second) k.second++;

        mouse_delta = 0;
    }

    void        set_mouse(cs::vec2 m)         { if (!mouse_disabled) mouse = m; }
    void        set_mouse_delta(cs::vec2 md)   { if(!mouse_disabled) mouse_delta = md;}

    void        disable_mouse(bool disable) { mouse_disabled = disable; }

    cs::vec2 get_mouse() { return mouse; }
    cs::vec2 get_mouse_delta() { return mouse_delta; }
    
    void press_key(uint32_t key) {
        key_press_map[key] = true;
        key_release_map[key] = false;
    }

    void release_key(uint32_t key) {
        key_press_map[key] = false;
        key_release_map[key] = true;
    }

    uint32_t is_key_pressed(uint32_t key) {
        return key_press_map[key];
    }
    
    bool is_key_released(uint32_t key) {
        return key_release_map[key];
    }
}