#pragma once
#include <cstdint>
#include <cstdlib>

#ifndef _WIN32
#ifndef linux
    #define linux
#endif
#endif

#ifdef linux
    #include <unistd.h>
#elif defined _WIN32
    #include <io.h>
    #include <share.h>
#endif

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

namespace cs {
    enum FILE_OPERATION {
        READ        = O_RDONLY,
        WRITE       = O_WRONLY,
        READ_WRITE  = O_RDWR,
        CREATE      = O_CREAT,
    };

    struct file{
        const char* path;   //So we can open the file later
        int file_descriptor;

        file(const char* path, uint32_t flags = O_RDONLY);
        ~file();
        
        bool is_open();

        void open(uint32_t flags);
        void close();
        
        void seek(uint32_t offset);

        int read(char* string, uint32_t len) {

            if (file::is_open())
#ifdef linux
                return ::read(file_descriptor, string, len);
#elif defined _WIN32
                return ::_read(file_descriptor, string, len);
#endif

            return -1;
        }

        template<typename data_type>
        int read(data_type* data) {

            if (file::is_open())
#ifdef linux
                return ::read(file_descriptor, data, sizeof(data_type));
#elif defined _WIN32
                return ::_read(file_descriptor, data, sizeof(data_type));
#endif

            return -1;
        }

        int write(const char* string, size_t length) {
            if (file::is_open())
#ifdef linux
                return ::write(file_descriptor, string, length);
#elif defined _WIN32
                return ::_write(file_descriptor, string, length);
#endif

            return -1;
        }

        template<typename data_type>
        int write(data_type *data) {
            if(file::is_open())
#ifdef linux
                return ::write(file_descriptor, data, sizeof(data_type));
#elif defined _WIN32
                return ::_write(file_descriptor, data, sizeof(data_type));
#endif
            
            return -1;
        }
    };
}