#pragma once

#include <vector>

#include "file.hpp"
#include <cs/renderer/vertex_array.hpp>
#include <cs/utility/log.hpp>

#ifdef linux
//Memory map
#include <sys/mman.h>
#endif

namespace cs {
    enum CSOF_Layout_e{
        CSOF_POSITION = 1U,
        CSOF_NORMAL = 2U,
        CSOF_TEXTURE_COORDINATE = 4U,
        CSOF_COLOR3  = 8U,
        CSOF_COLOR4  = 16U,
    };

    struct mesh {
        std::vector<float> vertices;
        std::vector<uint32_t> indices;
        
        cs::vertex_buffer   *vertex_buffer;
        cs::index_buffer    *index_buffer;
        
        cs::vertex_array    vertex_array;

        mesh() {}

        ~mesh() {
            delete vertex_buffer;
            delete index_buffer;
        }

        void init_vao(cs::buffer_layout layout) {
            vertex_buffer =  new cs::vertex_buffer(vertices.data(), vertices.size() * sizeof(float));
            vertex_buffer->set_layout(layout);
            vertex_array.add_vertex_buffer(vertex_buffer);

            index_buffer =  new cs::index_buffer(indices.data(), indices.size());
            vertex_array.set_index_buffer(index_buffer);
        }
    };

    // Todo: transformation matrices
    // Todo: !!!!animations!!!!
    bool load_csof_into_mesh(const char* path, cs::mesh& mesh)
    {
        int num_vertices;
        int vertex_size;
        int vertex_layout;
        int num_indices;
#ifdef linux
        int file_descriptor = open(path, O_RDONLY, 0666);
        
        //Size of file
        struct stat sb;
        fstat(file_descriptor, &sb);

        //Create memory map
        auto file_memory_map = mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, file_descriptor, 0);
        
        long file_index = 0;

        num_vertices = ((uint32_t*)file_memory_map)[file_index++];      
        vertex_size =  ((uint32_t*)file_memory_map)[file_index++];      
        vertex_layout =  ((uint32_t*)file_memory_map)[file_index++];
        num_indices =  ((uint32_t*)file_memory_map)[file_index++];      //16
        
        #ifdef DEBUG
            //cs::log::info("Loading: %s\n", path);
            //cs::log::comment("Vertices: %d\n", num_vertices);
            //cs::log::comment("Vertex_size: %d\n", vertex_size);
            //cs::log::comment("Vertex_layout: %d\n", vertex_layout);
            //cs::log::comment("Indices: %d\n", num_indices);
        #endif 

        mesh.vertices.reserve(num_vertices);
        for(auto n = 0; n < num_vertices; n++)                              //14*8*4=
        {
            for(auto vs = 0; vs < vertex_size; vs++)
                mesh.vertices.push_back(((float*)file_memory_map)[file_index++]);
        }
    
        mesh.indices.reserve(num_indices);
        for(auto n = 0; n < num_indices; n++) {
            mesh.indices.push_back(((uint32_t*)file_memory_map)[file_index++]);
        }

        munmap(file_memory_map, sb.st_size);
        close(file_descriptor);
#endif
#ifdef WIN32
		FILE* file = fopen(path, "rb");
		if (file != nullptr)
		{
			//File length
			fseek(file, 0, SEEK_END);
			auto length = ftell(file);
			rewind(file);

			//Read the file
			fread(&num_vertices, sizeof(int), 1, file);
			fread(&vertex_size, sizeof(int), 1, file);
			fread(&vertex_layout, sizeof(int), 1, file);
			fread(&num_indices, sizeof(int), 1, file);

			mesh.vertices.reserve(num_vertices);
			for (auto n = 0; n < num_vertices; n++)                              //14*8*4=
			{
				for (auto vs = 0; vs < vertex_size; vs++)
				{
					float v;
					fread(&v, sizeof(float), 1, file);
					mesh.vertices.emplace_back(v);
				}
			}

			mesh.indices.reserve(num_indices);
			for (auto n = 0; n < num_indices; n++) {
				{
					uint32_t v;
					fread(&v, sizeof(uint32_t), 1, file);
					mesh.indices.emplace_back(v);
				}
			}
		}
#endif
        buffer_layout layout;
        if (vertex_layout & CSOF_POSITION)
            layout.add_element({ shader_data_type::float3, "v_position" });
        if (vertex_layout & CSOF_NORMAL)
            layout.add_element({ shader_data_type::float3, "v_normal" });
        if (vertex_layout & CSOF_TEXTURE_COORDINATE)
            layout.add_element({ shader_data_type::float2, "v_texture_coordinate" });

        mesh.init_vao(layout);
        
        cs::log::mesh("Succesfully loaded \"%s\"", path);

        return true;
    }
}