#pragma once

#include <cstdarg>
#include <cstdio>
#include <vector>

namespace cs {
	namespace log {
		enum struct log_msg_category {
			INFO	= (1u << 0), 
			WARNING = (1u << 1), 
			ERROR	= (1u << 2),
			SHADER	= (1u << 3), 
			MESH	= (1u << 4), 
			TEXTURE = (1u << 5)
		};

		const char* log_msg_category_to_string(log_msg_category severity);

		struct log_msg {
			log_msg_category category;
			char text[512] = "";
		};

		void log_v(log_msg_category severity, const char* fmt, va_list args);

		void info(const char* fmt, ...);
		void warning(const char* fmt, ...);
		void error(const char* fmt, ...);

		/* Possibly one category -> resource ? */
		void shader(const char* fmt, ...);
		void mesh(const char* fmt, ...);
		void texture(const char* fmt, ...);

		std::vector<log_msg> get_log();
	}
}