#include "log.hpp"


namespace cs {
	namespace log {
		std::vector<log_msg> log_messages;

		const char* log_msg_category_to_string(log_msg_category severity) {
			switch (severity) {
			case log_msg_category::INFO:	return "INFO";
			case log_msg_category::WARNING: return "WARNING";
			case log_msg_category::ERROR:	return "ERROR";
			case log_msg_category::SHADER:	return "SHADER";
			case log_msg_category::MESH:	return "MESH";
			case log_msg_category::TEXTURE:	return "TEXTURE";
			default:						return "UNKNOWN";
			};
		}

		void log_v(log_msg_category severity, const char* fmt, va_list args) {
			log_msg new_msg;
			new_msg.category = severity;
			vsnprintf(new_msg.text, 512, fmt, args);

			log_messages.push_back(new_msg);
		}

		void info(const char* fmt, ...) {
			va_list args;
			va_start(args, fmt);
			log_v(log_msg_category::INFO, fmt, args);
			va_end(args);
		}

		void warning(const char* fmt, ...) {
			va_list args;
			va_start(args, fmt);
			log_v(log_msg_category::WARNING, fmt, args);
			va_end(args);
		}

		void error(const char* fmt, ...) {
			va_list args;
			va_start(args, fmt);
			log_v(log_msg_category::ERROR, fmt, args);
			va_end(args);
		}

		void shader(const char* fmt, ...) {
			va_list args;
			va_start(args, fmt);
			log_v(log_msg_category::SHADER, fmt, args);
			va_end(args);
		}

		void mesh(const char* fmt, ...) {
			va_list args;
			va_start(args, fmt);
			log_v(log_msg_category::MESH, fmt, args);
			va_end(args);
		}

		void texture(const char* fmt, ...) {
			va_list args;
			va_start(args, fmt);
			log_v(log_msg_category::TEXTURE, fmt, args);
			va_end(args);
		}

		std::vector<log_msg> get_log() {
			return log_messages;
		}
	}
}