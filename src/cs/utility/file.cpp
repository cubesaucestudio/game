#include "file.hpp"

#include <cstdio>

namespace cs {
    file::file(const char* path, uint32_t flags) :path(path) {
        file::open(flags);
    }

    file::~file() {
        file::close();
    }

    bool file::is_open() {
        return file_descriptor > 0;
    }

    void file::open(uint32_t flags) {  
#ifdef linux
        file_descriptor = ::open(this->path, flags, S_IRWXU);
#elif defined _WIN32
        _sopen_s(&file_descriptor, this->path, flags | O_BINARY, _SH_DENYNO, _S_IWRITE);
#endif
        if(!file::is_open()){ 
            printf("Error opening: %s\n", this->path);
        }
    }

    void file::close() {
        if(is_open()) 
#ifdef linux
            file_descriptor = ::close(file_descriptor);
#elif defined _WIN32
            file_descriptor = ::_close(file_descriptor);
#endif
    }

    void file::seek(uint32_t offset) {
        if(is_open()) 
#ifdef linux
            ::lseek(file_descriptor, offset, SEEK_SET);
#elif defined _WIN32
            ::_lseek(file_descriptor, offset, SEEK_SET);
#endif
    }
}