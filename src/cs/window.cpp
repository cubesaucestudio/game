#include "window.hpp"

#include <cstdio>

#include <cs/utility/io.hpp>

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if(action == GLFW_PRESS)
    {
        cs::press_key(key);
    }
    else if(action == GLFW_RELEASE)
    {
        //printf("R [%d]: %d\n", key, is_key_pressed(key));
        cs::release_key(key);
    }
    
}

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
    if(action == GLFW_PRESS)
        cs::press_key(button);
    else if(action == GLFW_RELEASE)
        cs::release_key(button);

}

static void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
    cs::vec2 pos = { float(xpos), float(ypos) };
    cs::set_mouse_delta(cs::get_mouse() - pos);
    cs::set_mouse(pos);
}

namespace cs {
    window::window(int width, int height, const char* title) : width(width), height(height){
        // Setup window
        if (!glfwInit()) return;

        // GL 3.0 + GLSL 130
        const char* glsl_version = "#version 130";
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
        //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
        //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only


        //GLFWmonitor* monitor = glfwGetPrimaryMonitor();
        //const GLFWvidmode* mode = glfwGetVideoMode(monitor);
        //
        //glfwWindowHint(GLFW_RED_BITS, mode->redBits);
        //glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
        //glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
        //glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

        // Create window with graphics context
        handle = glfwCreateWindow(width, height, title, NULL, NULL);
        if (handle == NULL) {
            return;
        }

        glfwMakeContextCurrent(handle);
        glfwSwapInterval(1); // Enable vsync

        glfwSetKeyCallback(handle, key_callback);
        glfwSetCursorPosCallback(handle, mouse_callback);
        glfwSetMouseButtonCallback(handle, mouse_button_callback);
        
        bool err = glewInit();
        if (err)
        {
            std::fprintf(stderr, "Failed to initialize OpenGL loader!\n");
            return;
        }
    }

    window::~window() {
        glfwDestroyWindow(handle);
        glfwTerminate();
    }

    void window::poll_events() {
        glfwPollEvents();
    }

    bool window::should_close() {
        return glfwWindowShouldClose(handle);
    }

    void window::close() {
        glfwSetWindowShouldClose(handle, true);
    }

    void window::update(float dt, float time) {
        // cs::input_update();
        glfwPollEvents();

        on_update(dt, time);
    }

    void window::render() {
        on_render();
    }
}