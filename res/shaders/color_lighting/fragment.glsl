#version 330 core
in vec3 p_normal;
in vec3 p_position;

out vec4 pixel_color;

uniform vec3 u_color;
uniform vec3 u_camera_position;

vec3 lightColor = vec3(1.0, 1.0, 1.0);
vec3 lightPos   = vec3(0, 15, 5);

void main()
{
    // ambient
    float ambientStrength = 0.2;
    vec3 ambient = ambientStrength * lightColor;
  	
    // diffuse 
    vec3 norm = normalize(p_normal);
    vec3 lightDir = normalize(lightPos - p_position);
    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;
            
    // specular
    float specularStrength = 0.1;
    vec3 viewDir = normalize(u_camera_position - p_position);
    vec3 reflectDir = reflect(-lightDir, norm);  
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * lightColor;  

    vec3 result = (ambient + diffuse + specular) * u_color;
    pixel_color = vec4(result, 1.0);
}