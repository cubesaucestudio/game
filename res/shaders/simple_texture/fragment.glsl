#version 330 core
in vec3 p_normal;
in vec3 p_position;
in vec2	p_texture_coordinate;

out vec4 pixel_color;

// texture sampler
uniform sampler2D albedo;

uniform vec3 u_color;
uniform vec3 u_camera_position;

void main()
{
	pixel_color = texture(albedo, p_texture_coordinate);
}