#version 330 core
in vec3 v_position;
in vec3 v_normal;
in vec2 v_texture_coordinate;

out vec3 p_position;
out vec3 p_normal;
out vec2 p_texture_coordinate;

uniform mat4 u_model;
uniform mat4 u_view_projection;

void main(){
    vec4 world_position = u_model * vec4(v_position, 1.0);
    p_position = vec3(world_position);
    p_normal = mat3(transpose(inverse(u_model))) * v_normal;  
    p_texture_coordinate = v_texture_coordinate;
    vec4 clip_position = u_view_projection * world_position;

    gl_Position = clip_position;
}
